
#include "MatchmakingLevelScript.h"

#include "Button.h"
#include "VerticalBox.h"
#include "TextBlock.h"
#include "TimerManager.h"
#include "SessionButton.h"
#include "VerticalBoxSlot.h"
#include "ScrollBox.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Misc/DateTime.h"


#include "UI/MatchmakingWidget.h"

AMatchmakingLevelScript::AMatchmakingLevelScript()
{
	ConstructorHelpers::FClassFinder<UUserWidget> MatchMakingBPClass(TEXT("/Game/Blueprints/Widgets/WBP_Matchmaking.WBP_Matchmaking_C"));
	if (!ensure(MatchMakingBPClass.Class != nullptr)) return;
	MatchmakingWidgetClass = MatchMakingBPClass.Class;
}

void AMatchmakingLevelScript::BeginPlay()
{
	Super::BeginPlay();

	bReadyToHost = false;
	ServersList = new TArray<FSessionInfo*>();

	//GetWorld()->GetTimerManager().SetTimer(ServerListTimerHandle, this, &AMatchmakingLevelScript::OnUpdateServerList, 2, true);

    if (MatchmakingWidgetClass) {
		MatchmakingWidget = CreateWidget<UUserWidget>(GetWorld(), MatchmakingWidgetClass);
		MatchmakingWidget->AddToViewport();

        auto ConnectButton = Cast<UButton>(MatchmakingWidget->GetWidgetFromName(TEXT("ConnectButton")));
		if (ConnectButton) {
		    ConnectButton->OnClicked.AddDynamic(this, &AMatchmakingLevelScript::OnConnectClicked);
		}

        auto HostButton = Cast<UButton>(MatchmakingWidget->GetWidgetFromName(TEXT("HostButton")));
		if (HostButton) {
			HostButton->SetIsEnabled(false);
			HostButton->OnClicked.AddDynamic(this, &AMatchmakingLevelScript::OnHostClicked);
		}

		ServerListScrollBoxWidget = Cast<UScrollBox>(MatchmakingWidget->GetWidgetFromName(TEXT("MyScrollBox")));
	}

    const auto Controller = GetWorld()->GetFirstPlayerController();
	if (Controller) {
		Controller->bShowMouseCursor = true;
		Controller->bEnableClickEvents = true;
		Controller->bEnableMouseOverEvents = true;
	}
}

void AMatchmakingLevelScript::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
}

void AMatchmakingLevelScript::StartGameHost(int Port)
{
	HostPort = Port;
	bReadyToHost = true;
}


void AMatchmakingLevelScript::UpdateSessionsList(FString ServerInfo)
{
	TArray<FString> Out;
	ServerInfo.ParseIntoArray(Out, TEXT("|"), true);
	for (int i = 1; i < Out.Num() - 3; i += 4) {
		FSessionInfo *TempInfo = new FSessionInfo();
		TempInfo->ID = FCString::Atoi(*Out[i]);
		TempInfo->Name = Out[i + 1];
		TempInfo->ServerIp = Out[i + 2];
		TempInfo->ServerPort = FCString::Atoi(*Out[i + 3]);

		bool sExist = false;
		for (int j = 0; j < ServersList->Num(); j++)
		{
			if ((*ServersList)[j]->ID == TempInfo->ID)
			{
				sExist = true;
			}
		}
		if (!sExist)
		{
			ServersList->Add(TempInfo);
		}

		/*if (ServersList)
		{
		}*/
	}
}

void AMatchmakingLevelScript::OnConnectClicked()
{
    TcpClient = new TCPClient(this);
}

void AMatchmakingLevelScript::OnHostClicked()
{
	FDateTime time;
	int32 CurrTimeHour = time.GetHour();
	int32 CurrTimeMin = time.GetMinute();
	int32 CurrTimeSec = time.GetSecond();
	FString timer = FString::FromInt(CurrTimeHour) + ":" + FString::FromInt(CurrTimeHour) + ":" + FString::FromInt(CurrTimeHour);

    TcpClient->HostNewGame("Hollow server " + timer, "7777");
}

void AMatchmakingLevelScript::OnUpdateServerList()
{
	if (TcpClient) {
		if (TcpClient->IsConnected()) {
			if (TcpClient->IsConnected())
			{
				UButton* connectButton = Cast<UButton>(MatchmakingWidget->GetWidgetFromName(TEXT("ConnectButton")));
				if (connectButton)
				{
					connectButton->SetIsEnabled(false);
				}
				UButton* hostButton = Cast<UButton>(MatchmakingWidget->GetWidgetFromName(TEXT("HostButton")));
				if (connectButton)
				{
					hostButton->SetIsEnabled(true);
				}
				TcpClient->RequestServerList();
				if (bReadyToHost)
				{
					APlayerController* pController = GetWorld()->GetFirstPlayerController();
					if (pController)
					{
						pController->ConsoleCommand("open FirstPersonExampleMap?Listen");
					}
				}
			}
		}
		if (ServersList->Num() >= 0) {
			if ((MatchmakingWidget) && (ServerListScrollBoxWidget)) {
				UE_LOG(LogTemp, Warning, TEXT("Message 661!!"));
				TArray<UWidget*> allchildren = ServerListScrollBoxWidget->GetAllChildren();

				for (int i = 0; i < allchildren.Num(); i++)
				{
					UE_LOG(LogTemp, Warning, TEXT("Message 663!!"));
					allchildren[i]->RemoveFromParent();

				}
				for (int i = 0; i < ServersList->Num(); i++)
				{
					UE_LOG(LogTemp, Warning, TEXT("Message 664!!"));

					UVerticalBox* itemWidgetBox = NewObject<UVerticalBox>();
					ServerListScrollBoxWidget->AddChild(itemWidgetBox);
					auto itemWidget = NewObject <USessionButton>(this);
					itemWidget->SetSessionInfo((*ServersList)[i]);
					UTextBlock* itemWidgetText = NewObject <UTextBlock>();
					itemWidgetText->SetText(FText::FromString((*ServersList)[i]->Name));
					itemWidget->AddChild(itemWidgetText);
					UVerticalBoxSlot* slot = itemWidgetBox->AddChildToVerticalBox(itemWidget);
					static FMargin Padding(5);
					slot->SetPadding(Padding);
				}
			}
		}
	}
}