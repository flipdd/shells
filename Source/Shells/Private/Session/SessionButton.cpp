// Fill out your copyright notice in the Description page of Project Settings.


#include "SessionButton.h"
#include "MatchmakingLevelScript.h"

USessionButton::USessionButton()
{
    OnClicked.AddDynamic(this, &USessionButton::OnClick);
}

void USessionButton::OnClick()
{
	APlayerController* pController = GetOuter()->GetWorld()->GetFirstPlayerController();
	if (pController)
	{
		UE_LOG(LogTemp, Warning, TEXT("Message sent!!"));
		FString cmd = "open " + this->SessionInfo->ServerIp + ":" + FString::FromInt(this->SessionInfo->ServerPort);
		pController->ConsoleCommand(cmd);
	}
}
void USessionButton::SetSessionInfo(FSessionInfo *Info)
{
    SessionInfo = Info;
}