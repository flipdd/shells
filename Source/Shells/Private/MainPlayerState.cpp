
#include "MainPlayerState.h"
#include "UnrealNetwork.h"
#include "Session\HTTPService.h"

void AMainPlayerState::InitPlayerSession(AHTTPService* HTTPService,
	FString hash, int id, int beastpoints, int beastdeaths, int mechapoints, int mechadeaths) {
	HTTPServiceRef = HTTPService;
	AuthenticationHash = hash;
	PID = id;
	TotalBeastPoints = beastpoints;
	TotalBeastDeaths = beastdeaths;
	TotalMechaPoints = mechapoints;
	TotalMechaDeaths = mechadeaths;
	UE_LOG(LogTemp, Warning, TEXT("Hash is /%s"), *hash);
	FString http = HTTPServiceRef->GetName();
	UE_LOG(LogTemp, Warning, TEXT("http name is: %s"), *http);
}

void AMainPlayerState::AddPlayerBeastPoint()
{
	TotalBeastPoints++;

	UE_LOG(LogTemp, Warning, TEXT("Added point"));

	if (HTTPServiceRef)
	{
		FAddBeastPointRequest AddPointInfo;
		AddPointInfo.hash = AuthenticationHash;
		AddPointInfo.userid = FString::FromInt(PID);
		HTTPServiceRef->AddBeastPoint(AddPointInfo);

		UE_LOG(LogTemp, Warning, TEXT("Add point info id : %d , hash %s"), *AddPointInfo.userid, *AddPointInfo.hash);
	}
}

void AMainPlayerState::AddPlayerBeastDeath()
{
	TotalBeastDeaths++;

	UE_LOG(LogTemp, Warning, TEXT("Added death"));

	if (HTTPServiceRef)
	{
		FAddBeastDeathRequest AddDeathInfo;
		AddDeathInfo.hash = AuthenticationHash;
		AddDeathInfo.userid = FString::FromInt(PID);
		HTTPServiceRef->AddBeastDeath(AddDeathInfo);

		UE_LOG(LogTemp, Warning, TEXT("Add point info id : %d , hash %s"), *AddDeathInfo.userid, *AddDeathInfo.hash);
	}
}

//mecha
void AMainPlayerState::AddPlayerMechaPoint()
{
	TotalMechaPoints++;

	UE_LOG(LogTemp, Warning, TEXT("Added point"));

	if (HTTPServiceRef)
	{
		FAddMechaPointRequest AddPointInfo;
		AddPointInfo.hash = AuthenticationHash;
		AddPointInfo.userid = FString::FromInt(PID);
		HTTPServiceRef->AddMechaPoint(AddPointInfo);

		UE_LOG(LogTemp, Warning, TEXT("Add point info id : %d , hash %s"), *AddPointInfo.userid, *AddPointInfo.hash);
	}
}

void AMainPlayerState::AddPlayerMechaDeath()
{
	TotalMechaDeaths++;

	UE_LOG(LogTemp, Warning, TEXT("Added death"));

	if (HTTPServiceRef)
	{
		FAddMechaDeathRequest AddDeathInfo;
		AddDeathInfo.hash = AuthenticationHash;
		AddDeathInfo.userid = FString::FromInt(PID);
		HTTPServiceRef->AddMechaDeath(AddDeathInfo);

		UE_LOG(LogTemp, Warning, TEXT("Add point info id : %d , hash %s"), *AddDeathInfo.userid, *AddDeathInfo.hash);
	}
}

void AMainPlayerState::GetLifetimeReplicatedProps(TArray<
	FLifetimeProperty>& OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AMainPlayerState, AuthenticationHash);
	DOREPLIFETIME(AMainPlayerState, TotalBeastPoints);
	DOREPLIFETIME(AMainPlayerState, TotalBeastDeaths);
	DOREPLIFETIME(AMainPlayerState, TotalMechaPoints);
	DOREPLIFETIME(AMainPlayerState, TotalMechaDeaths);
	DOREPLIFETIME(AMainPlayerState, PID);
}